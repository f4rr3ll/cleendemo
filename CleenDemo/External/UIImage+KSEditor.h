//
//  UIImage+Additions.h
//  CleenDemo
//
//  Created by iaknus on 2018/3/13.
//  Copyright © 2018年 IAKNUS STUDIO. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Color)

+ (UIImage *)imageWithColor:(UIColor *)color;

+ (UIImage *)imageWithColor:(UIColor *)color size:(CGSize)size;

+ (UIImage *)rectangleImageWithSize:(CGSize)size;

@end

@interface UIImage (Resize)

- (UIImage *)imageResizedToScale:(CGFloat)scale;

- (UIImage *)imageResizedToSize:(CGSize)size;

@end

@interface UIImage (Cropped)

- (UIImage *)imageCroppedToRect:(CGRect)rect;

- (UIImage *)squareImage;

- (UIImage *)circleImage;

@end

@interface UIImage (Merge)

+ (UIImage *)addImage:(UIImage *)image1 toImage:(UIImage *)image2;

@end

@interface UIImage (Rotate)

- (UIImage *)imageRotatedByDegrees:(CGFloat)degrees;

@end

@interface UIImage (ReplacementColor)

- (UIImage *)imageReplacementColor:(UIColor *)tintColor;

@end
