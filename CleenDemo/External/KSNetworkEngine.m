//
//  KSNetworkEngine.m
//  CleenDemo
//
//  Created by iaknus on 2018/3/13.
//  Copyright © 2018年 IAKNUS STUDIO. All rights reserved.
//

#import "KSNetworkEngine.h"
#import "YTKNetworkConfig.h"
#import "YTKNetworkAgent.h"
#import "KSConstants.h"


@implementation MSNetworkEngine

+ (void)setupService {
    YTKNetworkConfig *config = [YTKNetworkConfig sharedConfig];
    config.baseUrl = KSApiBaseURL;
    config.cdnUrl = KSApiCdnURL;
    
    [MSNetworkEngine converContentTypeConfig];
}

+ (void)converContentTypeConfig {
    YTKNetworkAgent *agent = [YTKNetworkAgent sharedAgent];
    NSSet *acceptableContentTypes = [NSSet setWithObjects:
                                     @"application/json",
                                     @"text/json",
                                     @"text/javascript",
                                     @"text/plain",
                                     @"text/html",
                                     @"text/css", nil];
    NSString *keypath = @"jsonResponseSerializer.acceptableContentTypes";
    [agent setValue:acceptableContentTypes forKeyPath:keypath];
}

@end
