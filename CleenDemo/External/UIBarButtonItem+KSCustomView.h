//
//  UIBarButtonItem+KSCustomView.h
//  CleenDemo
//
//  Created by iaknus on 2018/3/13.
//  Copyright © 2018年 IAKNUS STUDIO. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^TapActionBlock)(UIButton *button);

@interface UIBarButtonItem (KSCustomView)

@property(strong, nonatomic) UIButton *button;

+ (UIBarButtonItem *)barItemWithImage:(UIImage *)image;

+ (UIBarButtonItem *)barItemWithTitle:(NSString *)title;

- (void)addTapActionBlock:(TapActionBlock)block;


@end
