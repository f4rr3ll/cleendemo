//
//  UIView+KSLayout.h
//  CleenDemo
//
//  Created by iaknus on 2018/3/13.
//  Copyright © 2018年 IAKNUS STUDIO. All rights reserved.
//

#import <UIKit/UIKit.h>

CG_EXTERN CGPoint CGRectGetCenter(CGRect rect) CG_AVAILABLE_STARTING(__MAC_10_0, __IPHONE_2_0);

@interface UIView (KSLayout)

+ (instancetype)viewWithParent:(UIView *)parent;
- (instancetype)initWithParent:(UIView *)parent;
- (void)removeAllSubViews;

// Position of the top-left corner in superview's coordinates
@property (assign, nonatomic) CGPoint position;
@property (assign, nonatomic) CGFloat x;
@property (assign, nonatomic) CGFloat y;
@property (assign, nonatomic) CGFloat top;
@property (assign, nonatomic) CGFloat bottom;
@property (assign, nonatomic) CGFloat left;
@property (assign, nonatomic) CGFloat right;
@property(assign, nonatomic) CGFloat centerX;
@property(assign, nonatomic) CGFloat centerY;

// Makes hiding more logical
@property (assign, nonatomic) BOOL visible;

// Setting size keeps the position (top-left corner) constant
@property (assign, nonatomic) CGSize size;
@property (assign, nonatomic) CGFloat width;
@property (assign, nonatomic) CGFloat height;

// Border
- (void)setBorderWithEdge:(UIEdgeInsets)edge color:(UIColor *)color;

// Debug
- (void)showDebugBorder;

@end
