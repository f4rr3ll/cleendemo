//
//  UIColor+KSThemeColor.m
//  CleenDemo
//
//  Created by iaknus on 2018/3/13.
//  Copyright © 2018年 IAKNUS STUDIO. All rights reserved.
//

#import "UIColor+KSThemeColor.h"
#import "KSMacro.h"

@implementation UIColor (KSThemeColor)

+ (UIColor *)ks_themeColor {
    return HexColor(0x53cac4);
}

+ (UIColor *)ks_lightColor {
    return HexColor(0xffffff);
}

+ (UIColor *)ks_warningColor {
    return HexColor(0xfb3a3a);
}

+ (UIColor *)ks_textColor {
    return HexColor(0x3c4f84);
}

+ (UIColor *)ks_darkTextColor {
    return HexColor(0x333333);
}

+ (UIColor *)ks_lightTextColor {
    return HexColor(0x222222);
}

+ (UIColor *)ks_backgroundColor {
    return HexColor(0xf2f2f2);
}

+ (UIColor *)ks_lightBackgroundColor {
    return HexColor(0xffffff);
}

+ (UIColor *)ks_highlightedBackgroundColor {
    return HexColor(0xfafafa);
}

+ (UIColor *)ks_darkBackgroundColor {
    return HexColor(0x1d1f25);
}

+ (UIColor *)ks_imagePlaceholderColor {
    return HexColor(0xeaeaea);
}

+ (UIColor *)ks_separatorColor {
    return HexColor(0xe5e5e5);
}

@end
