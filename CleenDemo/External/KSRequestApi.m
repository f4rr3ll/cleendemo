//
//  KSRequestApi.m
//  CleenDemo
//
//  Created by iaknus on 2018/3/13.
//  Copyright © 2018年 IAKNUS STUDIO. All rights reserved.
//

#import "KSRequestApi.h"
#import "KSMacro.h"
#import "KSUtil.h"


@implementation KSRequestApi

- (instancetype)init {
    if (self = [super init]) {
        self.arguments = [NSMutableDictionary dictionary];
    }
    return self;
}

- (NSDictionary<NSString *,NSString *> *)requestHeaderFieldValueDictionary {
    NSMutableDictionary<NSString *,NSString *> *header = [NSMutableDictionary dictionary];
    
    return header;
}

- (id)jsonValidator {
    return @{ @"title": [NSString class], @"rows": [NSArray class] };
}

- (void)requestCompleteFilter {
    [self printNetworkLog:@"请求状态：成功"];
    
    NSError *error = nil;
    NSDictionary *data = [KSUtil parseResponseJSONObject:self.responseJSONObject error:&error];
    if (error == nil) {
        self.dataJSONObject = data;
    } else {
        self.dataJSONError = error;
    }
}

- (void)requestFailedFilter {
    [self printNetworkLog:[NSString stringWithFormat:@"请求状态：失败<%zd, %@>", self.responseStatusCode, self.error]];
}

- (NSMutableDictionary *)arguments {
    return _arguments;
}

- (void)printNetworkLog:(NSString *)log {
    NSString *method = self.requestMethod == YTKRequestMethodPOST ? @"POST" : @"GET";
    NSMutableString *logString = [NSMutableString stringWithFormat:@"[接口]%@", log];
    [logString appendFormat:@"\n%@：%@", method, self.originalRequest.URL];
    [logString appendFormat:@"\nHeader:%@", self.requestHeaderFieldValueDictionary];
    [logString appendFormat:@"\nArgument:%@", self.requestArgument];
    [logString appendFormat:@"\nResponse:%@", self.responseObject];
    DebugLog(@"%@", logString);
}

@end
