//
//  UIBarButtonItem+KSCustomView.m
//  CleenDemo
//
//  Created by iaknus on 2018/3/13.
//  Copyright © 2018年 IAKNUS STUDIO. All rights reserved.
//

#import "UIBarButtonItem+KSCustomView.h"
#import <objc/runtime.h>

static const void *ButtonKey = &ButtonKey;

@implementation UIBarButtonItem (KSCustomView)

static char key;

@dynamic button;

+ (UIBarButtonItem *)barItemWithImage:(UIImage *)image {
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:image forState:UIControlStateNormal];
    [button setFrame:CGRectMake(0, 0, MAX(image.size.width, 30), image.size.height)];
    
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    barButtonItem.button = button;
    return barButtonItem;
}

+ (UIBarButtonItem *)barItemWithTitle:(NSString *)title {
    NSDictionary *attributes = [UINavigationBar appearance].titleTextAttributes;
    UIColor *textColor = attributes[NSForegroundColorAttributeName];
    UIFont *textFont = attributes[NSFontAttributeName];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setFrame:CGRectMake(0, 0, 80, 40)];
    [button.titleLabel setFont:textFont];
    [button setTitle:title forState:UIControlStateNormal];
    [button setTitleColor:textColor forState:UIControlStateNormal];
    [button setContentHorizontalAlignment:UIControlContentHorizontalAlignmentRight];
    
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    barButtonItem.button = button;
    return barButtonItem;
}

- (void)addTapActionBlock:(TapActionBlock)block {
    [self.button addTarget:self action:@selector(tap:) forControlEvents:UIControlEventTouchUpInside];
    
    if (block) {
        objc_removeAssociatedObjects(self);
        objc_setAssociatedObject(self, &key, block, OBJC_ASSOCIATION_COPY);
    }
}

- (void)tap:(id)sender {
    TapActionBlock block = objc_getAssociatedObject(self, &key);
    if (block) {
        block(sender);
    }
}

- (UIButton *)button {
    return objc_getAssociatedObject(self, ButtonKey);
}

- (void)setButton:(UIButton *)button {
    objc_setAssociatedObject(self, ButtonKey, button, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

@end
