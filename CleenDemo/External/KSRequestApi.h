//
//  KSRequestApi.h
//  CleenDemo
//
//  Created by iaknus on 2018/3/13.
//  Copyright © 2018年 IAKNUS STUDIO. All rights reserved.
//

#import <YTKNetwork/YTKNetwork.h>

@interface KSRequestApi : YTKRequest

@property(copy, nonatomic) NSDictionary *dataJSONObject;

@property(copy, nonatomic) NSError *dataJSONError;

@property(strong, nonatomic) NSMutableDictionary *arguments;

@end
