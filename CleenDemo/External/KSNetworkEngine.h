//
//  KSNetworkEngine.h
//  CleenDemo
//
//  Created by iaknus on 2018/3/13.
//  Copyright © 2018年 IAKNUS STUDIO. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MSNetworkEngine : NSObject

/// 设置服务
+ (void)setupService;

@end
