//
//  UIImage+Additions.m
//  CleenDemo
//
//  Created by iaknus on 2018/3/13.
//  Copyright © 2018年 IAKNUS STUDIO. All rights reserved.
//

#import "UIImage+KSEditor.h"

@implementation UIImage (Color)

+ (UIImage *)imageWithColor:(UIColor *)color {
    return [self imageWithColor:color size:CGSizeMake(1.0f, 1.0f)];
}

+ (UIImage *)imageWithColor:(UIColor *)color size:(CGSize)size {
    UIGraphicsBeginImageContext(size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, CGRectMake(0, 0, size.width, size.height));

    UIImage *colorImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    return colorImage;
}

+ (UIImage *)rectangleImageWithSize:(CGSize)size {
    CGRect rect = CGRectMake(0, 0, size.width, size.height);

    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetRGBFillColor(context, 1.0f, 1.0f, 1.0f, 1.0f);
    CGContextFillRect(context, rect);
    CGContextSetRGBStrokeColor(context, 0, 0, 0, 1.0f);
    CGContextSetLineWidth(context, 0.1f);
    CGContextAddRect(context, rect);
    CGContextStrokePath(context);

    UIImage *rectangleImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    return rectangleImage;
}

@end

@implementation UIImage (Resize)

- (UIImage *)imageResizedToScale:(CGFloat)scale {
    CGSize size = CGSizeMake(self.size.width * scale, self.size.height * scale);
    return [self imageResizedToSize:size];
}

- (UIImage *)imageResizedToSize:(CGSize)size {
    UIGraphicsBeginImageContextWithOptions(size, NO, self.scale);
    [self drawInRect:CGRectMake(0.0, 0.0, size.width, size.height)];
    UIImage *resizedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    return resizedImage;
}

@end

@implementation UIImage (Cropped)

- (UIImage *)imageCroppedToRect:(CGRect)rect {
    CGImageRef imageRef = CGImageCreateWithImageInRect([self CGImage], rect);
    UIImage *croppedImage = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);

    return croppedImage;
}

- (UIImage *)squareImage {
    CGFloat min = self.size.width <= self.size.height ? self.size.width : self.size.height;
    return [self imageCroppedToRect:CGRectMake(0, 0, min, min)];
}

- (UIImage *)circleImage {
    UIGraphicsBeginImageContextWithOptions(self.size, NO, self.scale);
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    CGRect rect = CGRectMake(0, 0, self.size.width, self.size.height);
    CGContextAddEllipseInRect(ctx, rect);
    CGContextClip(ctx);
    [self drawInRect:rect];
    UIImage *resultImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return resultImage;
}

@end

@implementation UIImage (Merge)

+ (UIImage *)addImage:(UIImage *)image1 toImage:(UIImage *)image2 {
    UIGraphicsBeginImageContextWithOptions(image2.size, NO, [[UIScreen mainScreen] scale]);
    [image2 drawInRect:CGRectMake(0, 0, image2.size.width, image2.size.height)];

    CGFloat x = (image2.size.width - image1.size.width) / 2;
    CGFloat y = (image2.size.height - image1.size.height) / 2;
    [image1 drawInRect:CGRectMake(x, y, image1.size.width, image1.size.height)];

    UIImage *resultImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    return resultImage;
}

@end

@implementation UIImage (Rotate)

- (UIImage *)imageRotatedByDegrees:(CGFloat)degrees {
    CGRect rotatedViewRect = CGRectMake(0, 0, self.size.width, self.size.height);
    UIView *rotatedViewBox = [[UIView alloc] initWithFrame:rotatedViewRect];
    CGAffineTransform t = CGAffineTransformMakeRotation((CGFloat) (M_PI * degrees / 180.0f));
    rotatedViewBox.transform = t;
    CGSize rotatedSize = rotatedViewBox.frame.size;

    UIGraphicsBeginImageContextWithOptions(rotatedSize, NO, [[UIScreen mainScreen] scale]);
    CGContextRef bitmap = UIGraphicsGetCurrentContext();
    CGContextTranslateCTM(bitmap, rotatedSize.width / 2, rotatedSize.height / 2);
    CGContextRotateCTM(bitmap, (CGFloat) (M_PI * degrees / 180.0f));
    CGContextScaleCTM(bitmap, 1.0f, -1.0f);

    rotatedViewRect.origin.x = -self.size.width / 2;
    rotatedViewRect.origin.y = -self.size.height / 2;
    CGContextDrawImage(bitmap, rotatedViewRect, [self CGImage]);

    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    return newImage;
}

@end

@implementation UIImage (ReplacementColor)

- (UIImage *)imageReplacementColor:(UIColor *)tintColor {
    CGRect rect = CGRectMake(0, 0, self.size.width, self.size.height);
    UIGraphicsBeginImageContextWithOptions(self.size, NO, self.scale);
    CGContextRef c = UIGraphicsGetCurrentContext();
    [self drawInRect:rect];
    CGContextSetFillColorWithColor(c, [tintColor CGColor]);
    CGContextSetBlendMode(c, kCGBlendModeSourceAtop);
    CGContextFillRect(c, rect);
    UIImage *result = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    return result;
}

@end
