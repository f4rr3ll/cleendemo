//
//  UIColor+KSThemeColor.h
//  CleenDemo
//
//  Created by iaknus on 2018/3/13.
//  Copyright © 2018年 IAKNUS STUDIO. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (KSThemeColor)

/// 主题色
+ (UIColor *)ks_themeColor;

/// 通用白色
+ (UIColor *)ks_lightColor;
/// 警告红色
+ (UIColor *)ks_warningColor;

/// 文字颜色 - 正常
+ (UIColor *)ks_textColor;
/// 文字颜色 - 深色
+ (UIColor *)ks_darkTextColor;
/// 文字颜色 - 浅色
+ (UIColor *)ks_lightTextColor;

/// 视图背景色 - 正常
+ (UIColor *)ks_backgroundColor;
/// 视图背景色 - 浅色
+ (UIColor *)ks_lightBackgroundColor;
/// 视图背景色 - 高亮色
+ (UIColor *)ks_highlightedBackgroundColor;
/// 视图背景色 - 深色
+ (UIColor *)ks_darkBackgroundColor;

/// 图片占位图颜色
+ (UIColor *)ks_imagePlaceholderColor;

/// 分割线颜色
+ (UIColor *)ks_separatorColor;

@end
