//
//  UIView+KSLayout.m
//  CleenDemo
//
//  Created by iaknus on 2018/3/13.
//  Copyright © 2018年 IAKNUS STUDIO. All rights reserved.
//

#import "UIView+KSLayout.h"
#import "UIScreen+KSSize.h"
#import <QuartzCore/QuartzCore.h>

CG_EXTERN CGPoint CGRectGetCenter(CGRect rect) {
    return CGPointMake(CGRectGetMidX(rect), CGRectGetMidY(rect));
}

@implementation UIView (KSLayout)

+ (instancetype)viewWithParent:(UIView *)parent {
    return [[self alloc] initWithParent:parent];
}

- (instancetype)initWithParent:(UIView *)parent {
    if (self = [self initWithFrame:CGRectZero]) {
        if (parent && [parent isKindOfClass:[UIView class]]) {
            [parent addSubview:self];
        }
    }
    return self;
}

- (void)removeAllSubViews {
    for (UIView *subview in self.subviews){
        [subview removeFromSuperview];
    }
}

#pragma mark - Position of the top-left corner in superview's coordinates

- (CGPoint)position {
    return self.frame.origin;
}

- (void)setPosition:(CGPoint)position {
    CGRect rect = self.frame;
    rect.origin = position;
    [self setFrame:rect];
}

- (CGFloat)x {
    return self.frame.origin.x;
}

- (void)setX:(CGFloat)x {
    CGRect rect = self.frame;
    rect.origin.x = x;
    [self setFrame:rect];
}

- (CGFloat)y {
    return self.frame.origin.y;
}

- (void)setY:(CGFloat)y {
    CGRect rect = self.frame;
    rect.origin.y = y;
    [self setFrame:rect];
}

- (CGFloat)left {
    return self.frame.origin.x;
}

- (void)setLeft:(CGFloat)x {
    CGRect frame = self.frame;
    frame.origin.x = x;
    self.frame = frame;
}

- (CGFloat)top {
    return self.frame.origin.y;
}

- (void)setTop:(CGFloat)y {
    CGRect frame = self.frame;
    frame.origin.y = y;
    self.frame = frame;
}

- (CGFloat)right {
    return self.frame.origin.x + self.frame.size.width;
}

- (void)setRight:(CGFloat)right {
    CGRect frame = self.frame;
    frame.origin.x = right - frame.size.width;
    self.frame = frame;
}

- (CGFloat)bottom {
    return self.frame.origin.y + self.frame.size.height;
}

- (void)setBottom:(CGFloat)bottom {
    CGRect frame = self.frame;
    frame.origin.y = bottom - frame.size.height;
    self.frame = frame;
}

- (void)setCenterX:(CGFloat)centerX {
    self.center = CGPointMake(centerX, self.center.y);
}

- (void)setCenterY:(CGFloat)centerY {
    self.center = CGPointMake(self.center.x, centerY);
}

- (CGFloat)centerX {
    return self.center.x;
}

- (CGFloat)centerY {
    return self.center.y;
}

#pragma mark - Makes hiding more logical

- (BOOL)visible {
    return !self.hidden;
}

- (void)setVisible:(BOOL)visible {
    self.hidden = !visible;
}

#pragma mark - Setting size keeps the position (top-left corner) constant

- (CGSize)size {
    return [self frame].size;
}

- (void)setSize:(CGSize)size {
    CGRect rect = self.frame;
    rect.size = size;
    [self setFrame:rect];
}

- (CGFloat)width {
    return self.frame.size.width;
}

- (void)setWidth:(CGFloat)width {
    CGRect rect = self.frame;
    rect.size.width = width;
    [self setFrame:rect];
}

- (CGFloat)height {
    return self.frame.size.height;
}

- (void)setHeight:(CGFloat)height {
    CGRect rect = self.frame;
    rect.size.height = height;
    [self setFrame:rect];
}

#pragma mark - Border

- (void)setBorderWithEdge:(UIEdgeInsets)edge color:(UIColor *)color {
    CGFloat separatorFloat = 1.0f / [UIScreen scale];
    
    if (edge.top > 0) {
        CALayer *layer = [CALayer layer];
        CGFloat dip = edge.top >= 1 ?: separatorFloat;
        layer.frame = CGRectMake(0, 0, self.frame.size.width, dip);
        layer.backgroundColor = color.CGColor;
        [self.layer addSublayer:layer];
    }
    if (edge.left > 0) {
        CALayer *layer = [CALayer layer];
        CGFloat dip = edge.left >= 1 ?: separatorFloat;
        layer.frame = CGRectMake(0, 0, dip, self.frame.size.height);
        layer.backgroundColor = color.CGColor;
        [self.layer addSublayer:layer];
    }
    if (edge.bottom > 0) {
        CALayer *layer = [CALayer layer];
        CGFloat dip = edge.bottom >= 1 ?: separatorFloat;
        layer.frame = CGRectMake(0, self.height - dip, self.width, dip);
        layer.backgroundColor = color.CGColor;
        [self.layer addSublayer:layer];
    }
    if (edge.right > 0) {
        CALayer *layer = [CALayer layer];
        CGFloat dip = edge.right >= 1 ?: separatorFloat;
        layer.frame = CGRectMake(self.width - dip, 0, dip, self.height);
        layer.backgroundColor = color.CGColor;
        [self.layer addSublayer:layer];
    }
}

#pragma mark - Debug

- (void)showDebugBorder {
    self.layer.borderWidth = (1 / [UIScreen mainScreen].scale);
    self.layer.borderColor = [[UIColor blueColor] CGColor];
    
    for (UIView *subview in self.subviews) {
        subview.layer.borderWidth = (1 / [UIScreen mainScreen].scale);
        subview.layer.borderColor = [[UIColor redColor] CGColor];
    }
}

@end
