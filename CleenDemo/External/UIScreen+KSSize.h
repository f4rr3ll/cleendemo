//
//  UIScreen+KSSize.h
//  CleenDemo
//
//  Created by iaknus on 2018/3/13.
//  Copyright © 2018年 IAKNUS STUDIO. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIScreen (KSSize)

/// 屏幕宽度
+ (CGFloat)screenWidth;

/// 屏幕高度
+ (CGFloat)screenHeight;

/// 判断是否是retina屏幕
+ (BOOL)isRetina;

/// 屏幕比例因子
+ (CGFloat)scale;

@end
