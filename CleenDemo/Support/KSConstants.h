//
//  KSConstants.h
//  CleenDemo
//
//  Created by iaknus on 2018/3/13.
//  Copyright © 2018年 IAKNUS STUDIO. All rights reserved.
//

#ifndef KSConstants_h
#define KSConstants_h

#import <Foundation/Foundation.h>

#pragma mark - APP

static NSUInteger const KSAppStoreID = 123456789;

#pragma mark - API

static NSString * const KSApiBaseURL = @"http://thoughtworks-ios.herokuapp.com";
static NSString * const KSApiCdnURL = @"http://thoughtworks-ios.herokuapp.com";

#endif /* KSConstants_h */
