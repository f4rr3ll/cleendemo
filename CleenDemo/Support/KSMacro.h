//
//  KSMacro.h
//  CleenDemo
//
//  Created by iaknus on 2018/3/13.
//  Copyright © 2018年 IAKNUS STUDIO. All rights reserved.
//

#ifndef KSMacro_h
#define KSMacro_h

#pragma mark - Device & System

#define KS_BUNDLE_ID [[NSBundle mainBundle] bundleIdentifier]

#define KS_IDFV [[[UIDevice currentDevice] identifierForVendor] UUIDString]

#define KS_VERSION [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"]

#define KS_BUILD [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"]

#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)

#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)

#define IS_IPHONE_5_OR_LESS (IS_IPHONE && [[UIScreen mainScreen] bounds].size.height <= 568.0)


#pragma mark - Base Data

#define IS_EMPTY_STRING(__X) ([__X length] == 0)

#define INT_TO_STR(__X) [NSString stringWithFormat:@"%zd", __X]

#define IS_ARRAY(__X) ([__X isKindOfClass:[NSArray class]] && __X.count)

#define IS_DICTIONARY(__X) ([__X isKindOfClass:[NSDictionary class]] && __X.count)

#define SEPARATOR_FLOAT (1.0f / [UIScreen scale])   // 分割线宽度


#pragma mark - App Store URL

#define KSAppStoreURL [NSString stringWithFormat:@"itms-apps://itunes.apple.com/app/id%tu", KSAppStoreID]


#pragma mark - Random Number

#define KSRandom(_min, _max) [KSRandomHelper getRandomNumberWithRange:NSMakeRange(_min, _max - _min)]

#define KSRand(_num) KSRandom(0, _num)


#pragma mark - Document Path

#define kDocumentPath [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject]


#pragma mark - Widget

#define NAVIGATION_BAR_HEIGHT           64.0f
#define STATUS_BAR_HEIGHT               20.0f
#define TAB_BAR_HEIGHT                  49.0f
#define TOOLBAR_HEIGHT                  44.0f
#define DROP_DOWN_MENU_HEIGHT           40.0f

#define SCREEN_WIDTH                    [UIScreen screenWidth]
#define SCREEN_HEIGHT                   [UIScreen screenHeight]
#define SCREEN_SCALE                    [UIScreen scale]


#pragma mark - Color

#define HexColorWithAlpha(rgbValue, a)  [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
                                                        green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
                                                         blue:((float)(rgbValue & 0xFF))/255.0 \
                                                        alpha:(a)]
#define HexColor(rgbValue)              HexColorWithAlpha(rgbValue, 1.0)

#pragma mark - Log

#ifdef DEBUG
#   define DebugLog(...)    NSLog(__VA_ARGS__)
#   define DebugMethod()    NSLog(@"%s", __func__)
#   define DebugError()     NSLog(@"Error at %s Line:%d", __func__, __LINE__)
#else
#   define DebugLog(...)
#   define DebugMethod()
#   define DebugError()
#endif

#endif /* KSMacro_h */
