//
//  KSBaseViewController.m
//  CleenDemo
//
//  Created by iaknus on 2018/3/13.
//  Copyright © 2018年 IAKNUS STUDIO. All rights reserved.
//

#import "KSBaseViewController.h"
#import "KSMacro.h"
#import "UIScreen+KSSize.h"
#import "UIView+Toast.h"
#import "UIView+KSLayout.h"
#import "UIColor+KSThemeColor.h"


@implementation KSBaseViewController

- (void)dealloc {
    DebugLog(@"[%@ %@]", NSStringFromClass([self class]), NSStringFromSelector(_cmd));
}

#pragma mark - View lifecycle

- (void)loadView {
    [super loadView];
    
    self.view.backgroundColor = [UIColor ks_backgroundColor];
    self.prefersNavigationBarHidden = NO;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // 隐藏导航栏
    self.navigationController.fd_prefersNavigationBarHidden = self.prefersNavigationBarHidden;
    
    // 导航栏左边按钮
    if ([self.navigationController.viewControllers count] > 1) {
        __weak __typeof(self) weakSelf = self;
        UIBarButtonItem *backItem = [UIBarButtonItem barItemWithImage:[UIImage imageNamed:@"BackWhiteIcon"]];
        [backItem addTapActionBlock:^(UIButton *button) {
            [weakSelf handleBackAction:button];
        }];
        self.navigationItem.leftBarButtonItem = backItem;
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if (self.prefersNavigationBarHidden) {
        if ([self isBeingPresented] || [self isMovingToParentViewController]) {
            [self.navigationController setNavigationBarHidden:YES animated:NO];
        } else {
            if (self.navigationController.view.tag == 0) {
                [self.navigationController setNavigationBarHidden:YES animated:NO];
            } else {
                [self.navigationController setNavigationBarHidden:YES animated:animated];
            }
        }
        self.navigationController.view.tag = 0;
    } else {
        [self.navigationController setNavigationBarHidden:NO animated:animated];
        self.navigationController.view.tag = 1;
    }
}

#pragma mark - Push view controller

- (void)pushViewControllerWithName:(NSString *)viewControllerName withObject:(id)object {
    Class viewControllerClass = NSClassFromString(viewControllerName);
    UIViewController *destination = [[viewControllerClass alloc] init];
    destination.hidesBottomBarWhenPushed = YES;
    
    if (object) {
        SEL selector = NSSelectorFromString(@"setParams:");
        if ([destination respondsToSelector:selector]) {
            [destination setValue:object forKey:@"params"];
        }
    }
    [self.navigationController pushViewController:destination animated:YES];
}

#pragma mark - Handle press & tap action

- (void)handleBackAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)showFailureMessageWithError:(NSError *)error style:(KSFailureMessageDisplayStyle)style {
    if (error.code > 0) {
        NSString *message = error.localizedDescription;
        if (style == KSFailureMessageDisplayStyleAlert) {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:message preferredStyle:UIAlertControllerStyleAlert];
            [alertController addAction:[UIAlertAction actionWithTitle:@"好" style:UIAlertActionStyleDefault handler:nil]];
            [self presentViewController:alertController animated:YES completion:nil];
        } else {
            [self.view makeToast:message duration:3.0 position:CSToastPositionCenter];
        }
    }
}

#pragma mark - View Autorotate

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Custom accessors

- (void)setPrefersNavigationBarHidden:(BOOL)prefersNavigationBarHidden {
    _prefersNavigationBarHidden = prefersNavigationBarHidden;
    
    // 模拟edgesForExtendedLayout的设置
    CGFloat baseViewHeight = SCREEN_HEIGHT;
    if (!_prefersNavigationBarHidden) {
        baseViewHeight -= NAVIGATION_BAR_HEIGHT;
    }
    self.view.height = baseViewHeight;
}

@end
