//
//  KSBaseViewController.h
//  CleenDemo
//
//  Created by iaknus on 2018/3/13.
//  Copyright © 2018年 IAKNUS STUDIO. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIBarButtonItem+KSCustomView.h"
#import "UINavigationController+FDFullscreenPopGesture.h"

typedef NS_ENUM(NSInteger, KSFailureMessageDisplayStyle) {
    KSFailureMessageDisplayStyleToast,
    KSFailureMessageDisplayStyleAlert,
};

@interface KSBaseViewController : UIViewController <UINavigationControllerDelegate, UIImagePickerControllerDelegate>

@property(assign, nonatomic) BOOL prefersNavigationBarHidden;

@property(copy, nonatomic) NSObject *params;

- (void)handleBackAction:(id)sender;

- (void)showFailureMessageWithError:(NSError *)error style:(KSFailureMessageDisplayStyle)style;

- (void)pushViewControllerWithName:(NSString *)viewControllerName withObject:(id)object;

@end
