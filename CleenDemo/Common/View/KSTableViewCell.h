//
//  KSTableViewCell.h
//  CleenDemo
//
//  Created by iaknus on 2018/3/13.
//  Copyright © 2018年 IAKNUS STUDIO. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UITableView+FDTemplateLayoutCell.h"

@interface KSTableViewCell : UITableViewCell

@property(copy, nonatomic) UIColor *normalColor;

@property(copy, nonatomic) UIColor *selectedColor;

@property(copy, nonatomic) UIColor *highlightedColor;

@property(copy, nonatomic) NSArray<NSDictionary *> *extendLayouts;

@property(copy, nonatomic) void (^didSelectedCell)(KSTableViewCell *cell, BOOL selected);

@end
