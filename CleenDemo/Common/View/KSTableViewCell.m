//
//  KSTableViewCell.m
//  CleenDemo
//
//  Created by iaknus on 2018/3/13.
//  Copyright © 2018年 IAKNUS STUDIO. All rights reserved.
//

#import "KSTableViewCell.h"
#import "UIScreen+KSSize.h"
#import "UIView+KSLayout.h"
#import "UIColor+KSThemeColor.h"

@interface KSTableViewCell ()

@property(assign, nonatomic) UITableViewCellStyle style;

@end

@implementation KSTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.style = style;
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    if (self.imageView.image) {
        self.imageView.left = 14.0f;
        self.textLabel.left = self.imageView.right + 10.0f;
    } else {
        self.textLabel.left = 14.0f;
    }
    
    if (self.accessoryView) {
        self.accessoryView.right = [UIScreen screenWidth] - 14.0f;
    }
    
    if (self.style == UITableViewCellStyleSubtitle) {
        self.detailTextLabel.top = self.textLabel.bottom + 4.0f;
        self.detailTextLabel.left = self.textLabel.left;
    }
    
    if (self.style == UITableViewCellStyleValue1) {
        if (self.accessoryView) {
            self.detailTextLabel.right = self.accessoryView.left - 10.0f;
        } else {
            self.detailTextLabel.right = [UIScreen screenWidth] - 14.0f;
        }
    }
    if (self.style == UITableViewCellStyleValue2) {
        self.detailTextLabel.left = self.textLabel.right + 10.0f;
    }
    
    if (self.extendLayouts) {
        for (NSDictionary *layoutInfo in self.extendLayouts) {
            [layoutInfo enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
                [self setValue:obj forKeyPath:key];
            }];
        }
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    if (selected) {
        self.backgroundColor = self.selectedColor;
        self.accessoryView.backgroundColor = self.selectedColor;
    } else {
        self.backgroundColor = self.normalColor;
        self.accessoryView.backgroundColor = self.normalColor;
    }
    
    if (self.didSelectedCell) {
        self.didSelectedCell(self, selected);
    }
}

- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated {
    [super setHighlighted:highlighted animated:animated];
    
    if (highlighted) {
        self.backgroundColor = self.highlightedColor;
        self.accessoryView.backgroundColor = self.highlightedColor;
    } else {
        self.backgroundColor = self.normalColor;
        self.accessoryView.backgroundColor = self.normalColor;
    }
}

- (UIColor *)normalColor {
    if (_normalColor == nil) {
        _normalColor = [UIColor ks_lightBackgroundColor];
    }
    return _normalColor;
}

- (UIColor *)selectedColor {
    if (_selectedColor == nil) {
        _selectedColor = [UIColor ks_highlightedBackgroundColor];
    }
    return _selectedColor;
}

- (UIColor *)highlightedColor {
    if (_highlightedColor == nil) {
        _highlightedColor = [UIColor ks_highlightedBackgroundColor];
    }
    return _highlightedColor;
}

@end
