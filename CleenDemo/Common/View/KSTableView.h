//
//  KSTableView.h
//  CleenDemo
//
//  Created by iaknus on 2018/3/13.
//  Copyright © 2018年 IAKNUS STUDIO. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KSTableView : UITableView

#pragma mark - Empty Data Set

/// 空页面-标题
@property(copy, nonatomic) NSString *emptyDataTitle;
/// 空页面-副标题
@property(copy, nonatomic) NSString *emptyDataSubtitle;
/// 空页面-图片名称
@property(copy, nonatomic) NSString *emptyDataImageName;
/// 空页面-按钮标题
@property(copy, nonatomic) NSString *emptyDataButtonTitle;

@end
