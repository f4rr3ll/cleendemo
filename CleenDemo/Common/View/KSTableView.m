//
//  KSTableView.m
//  CleenDemo
//
//  Created by iaknus on 2018/3/13.
//  Copyright © 2018年 IAKNUS STUDIO. All rights reserved.
//

#import "KSTableView.h"
#import "KSMacro.h"
#import "UIView+KSLayout.h"
#import "UIColor+KSThemeColor.h"
#import <DZNEmptyDataSet/UIScrollView+EmptyDataSet.h>

@interface KSTableView () <DZNEmptyDataSetSource, DZNEmptyDataSetDelegate>

@property(assign, nonatomic, getter=isLoading) BOOL loading;

@end

@implementation KSTableView

- (id)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self setupTableView];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame style:(UITableViewStyle)style {
    if (self = [super initWithFrame:frame style:style]) {
        [self setupTableView];
    }
    return self;
}

- (void)setupTableView {
    self.backgroundColor = [UIColor ks_backgroundColor];
    self.separatorColor = [UIColor ks_separatorColor];
    self.showsVerticalScrollIndicator = NO;
    
    // 分割线从最左端开始
    if ([self respondsToSelector:@selector(setSeparatorInset:)]) {
        [self setSeparatorInset:UIEdgeInsetsMake(0, 14.0f, 0, 14.0f)];
    }
    if ([self respondsToSelector:@selector(setLayoutMargins:)]) {
        [self setLayoutMargins:UIEdgeInsetsMake(0, 14.0f, 0, 14.0f)];
    }
    
    // 清除UITableViewStylePlain样式中多余的分割线
    if (self.style == UITableViewStylePlain) {
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.width, 20)];
        view.backgroundColor = [UIColor clearColor];
        [self setTableFooterView:view];
    }
    
    self.emptyDataSetSource = self;
    self.emptyDataSetDelegate = self;
}

#pragma mark - DZNEmptyDataSet Methods

- (void)setLoading:(BOOL)loading {
    if (self.isLoading == loading) {
        return;
    }
    
    _loading = loading;
    
    [self reloadEmptyDataSet];
}

#pragma mark - DZNEmptyDataSetSource Methods

- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView {
    if (IS_EMPTY_STRING(self.emptyDataTitle)) {
        return nil;
    }
    
    NSMutableDictionary *attributes = [[NSMutableDictionary alloc] init];
    [attributes setObject:[UIFont boldSystemFontOfSize:17] forKey:NSFontAttributeName];
    [attributes setObject:HexColor(0x25282b) forKey:NSForegroundColorAttributeName];
    
    return [[NSAttributedString alloc] initWithString:self.emptyDataTitle attributes:attributes];
}

- (NSAttributedString *)descriptionForEmptyDataSet:(UIScrollView *)scrollView {
    if (IS_EMPTY_STRING(self.emptyDataSubtitle)) {
        return nil;
    }
    
    NSMutableDictionary *attributes = [[NSMutableDictionary alloc] init];
    
    NSMutableParagraphStyle *paragraph = [NSMutableParagraphStyle new];
    paragraph.lineBreakMode = NSLineBreakByWordWrapping;
    paragraph.alignment = NSTextAlignmentCenter;
    [attributes setObject:paragraph forKey:NSParagraphStyleAttributeName];
    [attributes setObject:[UIFont systemFontOfSize:14] forKey:NSFontAttributeName];
    [attributes setObject:HexColor(0x7b8994) forKey:NSForegroundColorAttributeName];
    
    return [[NSAttributedString alloc] initWithString:self.emptyDataSubtitle attributes:attributes];
}

- (UIImage *)imageForEmptyDataSet:(UIScrollView *)scrollView {
    if (self.isLoading) {
        return [UIImage imageNamed:@"loading_blue"];
    } else {
        return [UIImage imageNamed:self.emptyDataImageName];
    }
}

- (CAAnimation *)imageAnimationForEmptyDataSet:(UIScrollView *)scrollView {
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"transform"];
    animation.fromValue = [NSValue valueWithCATransform3D:CATransform3DIdentity];
    animation.toValue = [NSValue valueWithCATransform3D: CATransform3DMakeRotation(M_PI_2, 0.0, 0.0, 1.0) ];
    animation.duration = 0.25;
    animation.cumulative = YES;
    animation.repeatCount = MAXFLOAT;
    
    return animation;
}

- (NSAttributedString *)buttonTitleForEmptyDataSet:(UIScrollView *)scrollView forState:(UIControlState)state {
    if (IS_EMPTY_STRING(self.emptyDataButtonTitle)) {
        return nil;
    }
    
    NSMutableDictionary *attributes = [[NSMutableDictionary alloc] init];
    [attributes setObject:[UIFont systemFontOfSize:15] forKey:NSFontAttributeName];
    [attributes setObject:[UIColor ks_themeColor] forKey:NSForegroundColorAttributeName];
    
    return [[NSAttributedString alloc] initWithString:self.emptyDataButtonTitle attributes:attributes];
}

- (UIColor *)backgroundColorForEmptyDataSet:(UIScrollView *)scrollView {
    return self.backgroundColor;
}

- (CGFloat)verticalOffsetForEmptyDataSet:(UIScrollView *)scrollView {
    return -60.0f;
}

#pragma mark - DZNEmptyDataSetDelegate Methods

- (BOOL)emptyDataSetShouldDisplay:(UIScrollView *)scrollView {
    return YES;
}

- (BOOL)emptyDataSetShouldAllowTouch:(UIScrollView *)scrollView {
    return YES;
}

- (BOOL)emptyDataSetShouldAllowScroll:(UIScrollView *)scrollView {
    return YES;
}

- (BOOL)emptyDataSetShouldAnimateImageView:(UIScrollView *)scrollView {
    return self.isLoading;
}

- (void)emptyDataSet:(UIScrollView *)scrollView didTapView:(UIView *)view {
    self.loading = YES;
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        self.loading = NO;
    });
}

- (void)emptyDataSet:(UIScrollView *)scrollView didTapButton:(UIButton *)button {
    self.loading = YES;
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        self.loading = NO;
    });
}

@end
