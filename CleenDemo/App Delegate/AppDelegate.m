//
//  AppDelegate.m
//  CleenDemo
//
//  Created by iaknus on 2018/3/13.
//  Copyright © 2018年 IAKNUS STUDIO. All rights reserved.
//

#import "AppDelegate.h"
#import "KSNetworkEngine.h"
#import "KSRootViewController.h"

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Init Network
    [MSNetworkEngine setupService];
    
    self.window = [[UIWindow alloc] init];
    self.window.rootViewController = [[KSRootViewController alloc] init];
    [self.window makeKeyAndVisible];
    
    return YES;
}

@end
