//
//  KSGetFacesApi.m
//  CleenDemo
//
//  Created by iaknus on 2018/3/13.
//  Copyright © 2018年 IAKNUS STUDIO. All rights reserved.
//

#import "KSGetFacesApi.h"

@implementation KSGetFacesApi {
    NSString    *_pagingCursor;
}

- (instancetype)initWithPagingCursor:(NSString *)pagingCursor {
    if (self = [super init]) {
        _pagingCursor = pagingCursor;
    }
    return self;
}

- (NSString *)requestUrl {
    return @"/facts.json";
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodGET;
}

- (id)requestArgument {
    return self.arguments;
}

- (NSInteger)cacheTimeInSeconds {
    return 60 * 60 * 24;
}

- (BOOL)ignoreCache {
    return YES;
}

@end
