//
//  KSGetFacesApi.h
//  CleenDemo
//
//  Created by iaknus on 2018/3/13.
//  Copyright © 2018年 IAKNUS STUDIO. All rights reserved.
//

#import "KSRequestApi.h"

@interface KSGetFacesApi : KSRequestApi

- (instancetype)initWithPagingCursor:(NSString *)pagingCursor;

@end
