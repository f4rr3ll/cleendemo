//
//  KSFaces.m
//  CleenDemo
//
//  Created by iaknus on 2018/3/13.
//  Copyright © 2018年 IAKNUS STUDIO. All rights reserved.
//

#import "KSFaces.h"

@implementation KSFaces

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    NSMutableDictionary *dictionary = [[NSDictionary mtl_identityPropertyMapWithModel:self] mutableCopy];
    [dictionary setValue:@"description" forKey:@"detail"];
    [dictionary setValue:@"imageHref" forKey:@"imageUrl"];
    
    return dictionary;
}

@end
