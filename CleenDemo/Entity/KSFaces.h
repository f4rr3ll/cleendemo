//
//  KSFaces.h
//  CleenDemo
//
//  Created by iaknus on 2018/3/13.
//  Copyright © 2018年 IAKNUS STUDIO. All rights reserved.
//

#import <Mantle/Mantle.h>

@interface KSFaces : MTLModel <MTLJSONSerializing>

/// 标题
@property(copy, nonatomic) NSString *title;
/// 副标题
@property(copy, nonatomic) NSString *detail;
/// 图片链接
@property(copy, nonatomic) NSString *imageUrl;

@end
