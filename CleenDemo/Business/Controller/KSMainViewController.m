//
//  KSMainViewController.m
//  CleenDemo
//
//  Created by iaknus on 2018/3/13.
//  Copyright © 2018年 IAKNUS STUDIO. All rights reserved.
//

#import "KSMainViewController.h"
#import "KSMacro.h"
#import "KSFaces.h"
#import "KSGetFacesApi.h"
#import "KSTableView.h"
#import "KSTableViewCell.h"
#import "KSFeedTableViewCell.h"
#import "KSDataSourceProvider.h"
#import "MJRefreshStateHeader.h"
#import "MJRefreshBackStateFooter.h"
#import "UIView+KSLayout.h"


@interface KSMainViewController () <UITableViewDelegate, UITableViewDataSource, KSDataSourceProviderDelegate>

/// 列表
@property(strong, nonatomic) KSTableView *tableView;
/// 数据源
@property(strong, nonatomic) KSDataSourceProvider *dataSourceProvider;

@end

static NSString *cellReuseId = @"KSFeedTableViewCell";

@implementation KSMainViewController

- (void)loadView {
    [super loadView];
    
    self.title = @"Cleen";
    
    // 创建TableView
    [self.view addSubview:self.tableView];
    
    // 设置空白界面
    self.tableView.emptyDataTitle = @"Ooops!";
    self.tableView.emptyDataSubtitle = @"~ Network Error ~";
    self.tableView.emptyDataImageName = @"EmptyViewIcon";
    self.tableView.emptyDataButtonTitle = @"Refresh";
    
    // 注册Cell
    [self.tableView registerClass:[KSFeedTableViewCell class] forCellReuseIdentifier:cellReuseId];
    
    __weak __typeof(self) weakSelf = self;
    
    // 设置下拉刷新/上拉加载组件
    self.tableView.mj_header = [MJRefreshStateHeader headerWithRefreshingBlock:^{
        [weakSelf.dataSourceProvider handleRefreshAction];
    }];
    self.tableView.mj_footer = [MJRefreshBackStateFooter footerWithRefreshingBlock:^{
        [weakSelf.dataSourceProvider handleLoadMoreAction];
    }];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // 初始化数据源
    self.dataSourceProvider = [[KSDataSourceProvider alloc] init];
    self.dataSourceProvider.delegate = self;
    
    // 加载缓存数据
    [self.dataSourceProvider loadRequestCache];
    // 请求首页数据
    [self.dataSourceProvider handleRefreshAction];
}

- (void)dealloc {
    self.tableView.delegate = nil;
    self.tableView.dataSource = nil;
}

#pragma mark - Config Cell

- (void)configCell:(KSFeedTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    cell.fd_enforceFrameLayout = YES;
    
    if (indexPath.row < self.dataSourceProvider.dataSource.count) {
        [cell loadFeedData:self.dataSourceProvider.dataSource[indexPath.row]];
    }
}

#pragma mark - UITableViewDelegate & UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataSourceProvider.dataSource.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [tableView fd_heightForCellWithIdentifier:cellReuseId cacheByIndexPath:indexPath configuration:^(id cell) {
        [self configCell:cell atIndexPath:indexPath];
    }];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    id cell = [tableView dequeueReusableCellWithIdentifier:cellReuseId forIndexPath:indexPath];
    [self configCell:cell atIndexPath:indexPath];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // 取消选中
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    // 跳转
    KSFaces *faces = self.dataSourceProvider.dataSource[indexPath.row];
    [self pushViewControllerWithName:@"KSDetailViewController" withObject:faces.title];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    // 倒数第五条预加载
    if (indexPath.row == self.dataSourceProvider.dataSource.count - 5) {
        [self.tableView.mj_footer endRefreshingWithNoMoreData];
    }
}

#pragma mark - KSDataSourceProviderDelegate

- (KSRequestApi *)requestApiWithCurrentPagingCursor:(NSString *)pagingCursor {
    return [[KSGetFacesApi alloc] initWithPagingCursor:pagingCursor];
}

- (NSArray *)modelsTransformerFromfromJSONDictionary:(NSDictionary *)dictionary {
    NSArray *array = [MTLJSONAdapter modelsOfClass:[KSFaces class] fromJSONArray:dictionary[@"rows"] error:nil];
    NSMutableArray *modelsArray = [NSMutableArray arrayWithCapacity:array.count];
    for (KSFaces *faces in array) {
        if (faces.title.length > 0) {
            [modelsArray addObject:faces];
        }
    }
    
    self.title = dictionary[@"title"];
    
    return modelsArray;
}

- (NSString *)updatePagingCursorWithLatestObject:(NSObject *)object {
    return nil;
}

- (void)refreshComplete:(KSRefreshResult)refreshResult {
    [self.tableView.mj_header endRefreshing];
    [self.tableView.mj_footer endRefreshing];
    
    if (refreshResult == KSRefreshResultNoMoreData) {
        [self.tableView.mj_footer endRefreshingWithNoMoreData];
    }
    if (refreshResult == KSRefreshResultSuccess) {
        [self.tableView reloadData];
    }
}

#pragma mark - Custom accessors

- (UITableView *)tableView {
    if (_tableView == nil) {
        _tableView = [[KSTableView alloc] initWithFrame:self.view.bounds];
        _tableView.delegate = self;
        _tableView.dataSource = self;
    }
    return _tableView;
}

@end
