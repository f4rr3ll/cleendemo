//
//  KSDetailViewController.m
//  CleenDemo
//
//  Created by iaknus on 2018/3/14.
//  Copyright © 2018年 IAKNUS STUDIO. All rights reserved.
//

#import "KSDetailViewController.h"

#import "KSTableView.h"
#import "KSTableViewCell.h"

@interface KSDetailViewController () <UITableViewDelegate, UITableViewDataSource>

@property(strong, nonatomic) KSTableView *tableView;

@end

static NSString *cellReuseId = @"KSDetailTableViewCell";

@implementation KSDetailViewController

- (void)loadView {
    [super loadView];
    
    self.title = @"Details";
    
    // 创建TableView
    self.tableView = [[KSTableView alloc] initWithFrame:self.view.bounds];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.view addSubview:self.tableView];
    
    self.tableView.emptyDataTitle = [NSString stringWithFormat:@"%@", self.params];
    self.tableView.emptyDataSubtitle = @"Favorites are saved for offline access.";
    self.tableView.emptyDataImageName = @"EmptyViewIcon";
    self.tableView.emptyDataButtonTitle = @"Lean more";
    
    // 注册Cell
    [self.tableView registerClass:[KSTableViewCell class] forCellReuseIdentifier:cellReuseId];
}

- (void)viewDidLoad {
    [super viewDidLoad];
}

#pragma mark - UITableViewDelegate & UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 54.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    id cell = [tableView dequeueReusableCellWithIdentifier:cellReuseId forIndexPath:indexPath];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // 取消选中
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

@end
