//
//  KSRootViewController.m
//  CleenDemo
//
//  Created by iaknus on 2018/3/13.
//  Copyright © 2018年 IAKNUS STUDIO. All rights reserved.
//

#import "KSRootViewController.h"
#import "KSMainViewController.h"
#import "KSMacro.h"
#import "UIScreen+KSSize.h"
#import "UIView+KSLayout.h"
#import "UIImage+KSEditor.h"
#import "UIColor+KSThemeColor.h"


@implementation KSRootViewController

#pragma mark - View lifecycle

- (void)loadView {
    [super loadView];
    
    // 定制外观
    [self customAppearance];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // 设置默认视图
    [self pushViewController:[KSMainViewController new] animated:NO];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Appearance

- (void)customAppearance {
    // Navigation Bar
    [[UINavigationBar appearance] setTranslucent:NO];
    [[UINavigationBar appearance] setTintColor:[UIColor ks_lightColor]];
    [[UINavigationBar appearance] setBarTintColor:[UIColor ks_themeColor]];
    [[UINavigationBar appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor ks_lightColor],
                                                           NSFontAttributeName:[UIFont systemFontOfSize:17.0f]}];
    [[UINavigationBar appearance] setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    [[UINavigationBar appearance] setShadowImage:[UIImage new]];
    
    // Tab Bar
    [[UITabBar appearance] setTranslucent:NO];
    [[UITabBar appearance] setTintColor:[UIColor ks_themeColor]];
    [[UITabBar appearance] setBackgroundImage:[UIImage imageWithColor:[UIColor ks_lightColor]
                                                                 size:CGSizeMake(SCREEN_WIDTH, TAB_BAR_HEIGHT)]];
    [[UITabBar appearance] setShadowImage:[UIImage imageWithColor:[UIColor ks_separatorColor]
                                                             size:CGSizeMake(SCREEN_WIDTH, SEPARATOR_FLOAT)]];
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

@end
