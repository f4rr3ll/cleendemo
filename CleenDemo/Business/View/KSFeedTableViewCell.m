//
//  KSFeedTableViewCell.m
//  CleenDemo
//
//  Created by iaknus on 2018/3/13.
//  Copyright © 2018年 IAKNUS STUDIO. All rights reserved.
//

#import "KSFeedTableViewCell.h"
#import "KSUtil.h"
#import "KSMacro.h"
#import "UIScreen+KSSize.h"
#import "UIView+KSLayout.h"
#import "UIColor+KSThemeColor.h"
#import "UIImageView+WebCache.h"


@interface KSFeedTableViewCell ()

@property(strong, nonatomic) KSFaces *faces;

@end

@implementation KSFeedTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:reuseIdentifier]) {
        // 标题
        self.textLabel.font = [UIFont systemFontOfSize:16.0f];
        self.textLabel.textColor = [UIColor ks_textColor];
        self.textLabel.numberOfLines = 0;
        // 副标题
        self.detailTextLabel.font = [UIFont systemFontOfSize:14.0f];
        self.detailTextLabel.textColor = [UIColor ks_lightTextColor];
        self.detailTextLabel.numberOfLines = 0;
        // 图片
        self.imageView.contentMode = UIViewContentModeScaleAspectFit;
        // 向右箭头
        self.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    return self;
}

#pragma mark - Override methods

- (void)layoutSubviews {
    [super layoutSubviews];
    
    // 调整组件大小
    [self sizeToFitAllLabels];
    [self sizeToFitImageView];
    
    // 组件布局
    self.textLabel.left = 14.0f;
    self.textLabel.top = 10.0f;
    self.detailTextLabel.left = self.textLabel.left;
    self.detailTextLabel.top = self.textLabel.bottom + 6.0f;
    self.imageView.top = self.textLabel.bottom + 6.0f;
    self.imageView.right = SCREEN_WIDTH - 40;
}

- (CGSize)sizeThatFits:(CGSize)size {
    [self sizeToFitAllLabels];
    [self sizeToFitImageView];
    
    CGFloat totalHeight = 10.0f;
    if (self.textLabel.text.length > 0) {
        totalHeight += self.textLabel.height + 6.0;
    }
    
    CGFloat dynamicHeight = MAX(self.detailTextLabel.height, self.imageView.height);
    if (dynamicHeight > 0) {
        totalHeight += dynamicHeight + 10.0;
    }
    
    return CGSizeMake(size.width, totalHeight);
}

#pragma mark - Load Data

- (void)loadFeedData:(KSFaces *)faces {
    self.faces = faces;
    
    self.textLabel.text = faces.title;
    self.detailTextLabel.text = [KSUtil replaceUnicode:faces.detail];
    
    if (self.faces.imageUrl.length > 4) {
        [self.imageView sd_setImageWithURL:[NSURL URLWithString:faces.imageUrl]
                          placeholderImage:[UIImage imageNamed:@"ImgPlaceholder"]
                                 completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                     if (cacheType == SDImageCacheTypeNone) {
                                         //TODO: 刷新该Cell
                                     }
                                 }];
    } else {
        self.imageView.image = nil;
    }
}

#pragma mark - Private methods

- (void)sizeToFitAllLabels {
    self.textLabel.width = SCREEN_WIDTH - 14.0 - 30.0;
    self.detailTextLabel.width = self.textLabel.width;
    
    if (self.faces.imageUrl.length > 0) {
        self.detailTextLabel.width = SCREEN_WIDTH - 14.0 - 160.0;
    }
    
    [self.textLabel sizeToFit];
    [self.detailTextLabel sizeToFit];
}

- (void)sizeToFitImageView {
    CGFloat imageWidth = 100.0f;
    CGFloat imageHeight = 40.0f;
    
    UIImage *image = self.imageView.image;
    if (self.faces.imageUrl.length > 4) {
        if (image) {
            imageHeight = image.size.height * (imageWidth / image.size.width);
        }
    } else {
        imageHeight = 0.0f;
    }
    
    self.imageView.size = CGSizeMake(imageWidth, imageHeight);
}

@end
