//
//  KSFeedTableViewCell.h
//  CleenDemo
//
//  Created by iaknus on 2018/3/13.
//  Copyright © 2018年 IAKNUS STUDIO. All rights reserved.
//

#import "KSTableViewCell.h"
#import "KSFaces.h"

@interface KSFeedTableViewCell : KSTableViewCell

- (void)loadFeedData:(KSFaces *)faces;

@end
