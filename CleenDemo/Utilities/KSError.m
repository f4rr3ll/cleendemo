//
//  KSError.m
//  CleenDemo
//
//  Created by iaknus on 2018/3/13.
//  Copyright © 2018年 IAKNUS STUDIO. All rights reserved.
//

#import "KSError.h"

NSErrorDomain const KSErrorDomain = @"com.iaknus.KSErrorDomain";

@implementation KSError

+ (NSError *)errorCode:(KSErrorCode)errorCode {
    NSMutableDictionary *userInfo = [NSMutableDictionary dictionary];
    
    if (labs(errorCode) / 1000 == 1) {
        userInfo[NSLocalizedFailureReasonErrorKey] = @"无法提供服务";
        userInfo[NSLocalizedRecoverySuggestionErrorKey] = @"请检查网络";
        
        if (errorCode == KSNetworkError) {
            userInfo[NSLocalizedDescriptionKey] = @"网络错误";
        } else if (errorCode == KSResponseInvalidFormatError) {
            userInfo[NSLocalizedDescriptionKey] = @"无法获取数据";
        }
    }
    
    if (labs(errorCode) / 1000 == 2) {
        userInfo[NSLocalizedFailureReasonErrorKey] = @"接口返回数据校验失败";
        userInfo[NSLocalizedRecoverySuggestionErrorKey] = @"请检查接口";
        
        if (errorCode == KSResponseNoResultError) {
            userInfo[NSLocalizedDescriptionKey] = @"接口没有返回数据";
        } else if (errorCode == KSResponseInvalidFormatError) {
            userInfo[NSLocalizedDescriptionKey] = @"接口返回数据类型错误";
        } else if (errorCode == KSResponseCodeError) {
            userInfo[NSLocalizedDescriptionKey] = @"接口使用了非法的错误码";
        } else if (errorCode == KSResponseReadDataError) {
            userInfo[NSLocalizedDescriptionKey] = @"接口返回数据读取错误";
        } else {
            userInfo[NSLocalizedDescriptionKey] = @"未知错误";
        }
    }
    
    return [NSError errorWithDomain:KSErrorDomain code:errorCode userInfo:userInfo];
}

@end
