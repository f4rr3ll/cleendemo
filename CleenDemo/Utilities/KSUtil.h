//
//  KSUtil.h
//  CleenDemo
//
//  Created by iaknus on 2018/3/13.
//  Copyright © 2018年 IAKNUS STUDIO. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KSUtil : NSObject

/// 解析网络请求数据，返回字典类型
+ (NSDictionary *)parseResponseJSONObject:(id)responseJSONObject error:(NSError **)errorPtr;

/// 处理Unicode码
+ (NSString *)replaceUnicode:(NSString *)unicodeStr;

@end
