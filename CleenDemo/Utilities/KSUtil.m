//
//  KSUtil.m
//  CleenDemo
//
//  Created by iaknus on 2018/3/13.
//  Copyright © 2018年 IAKNUS STUDIO. All rights reserved.
//

#import "KSUtil.h"
#import "KSError.h"

@implementation KSUtil

+ (NSDictionary *)parseResponseJSONObject:(id)responseJSONObject error:(NSError **)errorPtr {
    NSDictionary *dictionary = [NSDictionary dictionary];
    
    if (![responseJSONObject isKindOfClass:[NSDictionary class]]) {
        *errorPtr = [KSError errorCode:KSResponseInvalidFormatError];
        return dictionary;
    }
    
    if ([responseJSONObject count] == 0) {
        *errorPtr = [KSError errorCode:KSResponseNoResultError];
        return dictionary;
    }
    
    if (![responseJSONObject[@"rows"] isKindOfClass:[NSArray class]]) {
        *errorPtr = [KSError errorCode:KSResponseReadDataError];
        return dictionary;
    }
    
    dictionary = [responseJSONObject copy];
    
    return dictionary;
}

+ (NSString *)replaceUnicode:(NSString *)unicodeStr {
    if (unicodeStr.length == 0) {
        return @"";
    }
    
    NSString *tempStr1 = [unicodeStr stringByReplacingOccurrencesOfString:@"\\u"withString:@"\\U"];
    NSString *tempStr2 = [tempStr1 stringByReplacingOccurrencesOfString:@"\""withString:@"\\\""];
    NSString *tempStr3 = [[@"\""stringByAppendingString:tempStr2] stringByAppendingString:@"\""];
    NSData *stringData = [tempStr3 dataUsingEncoding:NSUTF8StringEncoding];
    if (stringData == nil) {
        return unicodeStr;
    }
    NSString *returnStr = [NSPropertyListSerialization propertyListWithData:stringData options:NSPropertyListImmutable format:NULL error:nil];
    return [returnStr stringByReplacingOccurrencesOfString:@"\\r\\n"withString:@"\n"];
}

@end
