//
//  KSRandomHelper.m
//  CleenDemo
//
//  Created by iaknus on 2018/3/13.
//  Copyright © 2018年 IAKNUS STUDIO. All rights reserved.
//

#import "KSRandomHelper.h"

@implementation KSRandomHelper

+ (NSInteger)getRandomNumberWithRange:(NSRange)range {
    return (NSInteger)(range.location + (arc4random() % range.length));
}

@end
