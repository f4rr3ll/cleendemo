//
//  KSDataSourceProvider.h
//  CleenDemo
//
//  Created by iaknus on 2018/3/13.
//  Copyright © 2018年 IAKNUS STUDIO. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KSRequestApi.h"


typedef NS_ENUM(NSInteger, KSRequestActionType) {
    KSRequestActionTypeRefresh = 0,
    KSRequestActionTypeLoadMore,
};

typedef NS_ENUM(NSInteger, KSRefreshResult) {
    KSRefreshResultFailure,
    KSRefreshResultSuccess,
    KSRefreshResultNoMoreData,
};

typedef void (^KSRefreshComplete)(KSRefreshResult result);

@protocol KSDataSourceProviderDelegate <NSObject>

- (KSRequestApi *)requestApiWithCurrentPagingCursor:(NSString *)pagingCursor;

- (NSArray *)modelsTransformerFromfromJSONDictionary:(NSDictionary *)dictionary;

- (NSString *)updatePagingCursorWithLatestObject:(NSObject *)object;

- (void)refreshComplete:(KSRefreshResult)refreshResult;

@end

@interface KSDataSourceProvider : NSObject

/// 代理对象
@property(weak, nonatomic) id<KSDataSourceProviderDelegate> delegate;

/// 请求类型
@property(assign, nonatomic) KSRequestActionType requestActionType;
/// 数据集合
@property(strong, nonatomic) NSMutableArray *dataSource;
/// 正在请求
@property(assign, nonatomic) BOOL isRequestingData;
/// 分页游标
@property(strong, nonatomic) NSString *pagingCursor;

/// 加载缓存数据
- (void)loadRequestCache;
/// 刷新数据
- (void)handleRefreshAction;
/// 加载更多数据
- (void)handleLoadMoreAction;

@end
