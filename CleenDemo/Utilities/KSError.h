//
//  KSError.h
//  CleenDemo
//
//  Created by iaknus on 2018/3/13.
//  Copyright © 2018年 IAKNUS STUDIO. All rights reserved.
//

#import <Foundation/Foundation.h>

FOUNDATION_EXPORT NSErrorDomain const KSErrorDomain;

typedef NS_ENUM(NSInteger, KSErrorCode) {
    KSNetworkError                      = 1000,
    KSServiceError                      = 1001,
    
    KSResponseUnknownError              = 2000,
    KSResponseNoResultError             = 2001,
    KSResponseInvalidFormatError        = 2002,
    KSResponseCodeError                 = 2003,
    KSResponseReadDataError             = 2004,
};

@interface KSError : NSObject

+ (NSError *)errorCode:(KSErrorCode)errorCode;

@end
