//
//  KSDataSourceProvider.m
//  CleenDemo
//
//  Created by iaknus on 2018/3/13.
//  Copyright © 2018年 IAKNUS STUDIO. All rights reserved.
//

#import "KSDataSourceProvider.h"
#import "KSUtil.h"

@implementation KSDataSourceProvider

#pragma mark - Load Cache

- (void)loadRequestCache {
    KSRequestApi *api;
    if ([self.delegate respondsToSelector:@selector(requestApiWithCurrentPagingCursor:)]) {
        api = [self.delegate requestApiWithCurrentPagingCursor:nil];
    }
    
    if (api == nil) return;
    
    if ([api loadCacheWithError:nil]) {
        NSError *error = nil;
        NSDictionary *data = [KSUtil parseResponseJSONObject:api.responseJSONObject error:&error];
        if (error == nil) {
            NSArray *array;
            if ([self.delegate respondsToSelector:@selector(modelsTransformerFromfromJSONDictionary:)]) {
                array = [self.delegate modelsTransformerFromfromJSONDictionary:data];
            }
            if (array.count > 0) {
                self.dataSource = [array mutableCopy];
                if ([self.delegate respondsToSelector:@selector(refreshComplete:)]) {
                    [self.delegate refreshComplete:KSRefreshResultSuccess];
                }
            }
        }
    }
}

#pragma mark - Handle Refresh & Load more Action

- (void)handleRefreshAction {
    self.pagingCursor = nil;
    
    __weak __typeof(self) weakSelf = self;
    [self requestApiDataWithActionType:KSRequestActionTypeRefresh refreshComplete:^(KSRefreshResult result) {
        __strong __typeof(weakSelf) strongSelf = weakSelf;
        if ([strongSelf.delegate respondsToSelector:@selector(refreshComplete:)]) {
            [strongSelf.delegate refreshComplete:result];
        }
    }];
}

- (void)handleLoadMoreAction {
    if (self.isRequestingData) {
        return;
    }
    
    self.isRequestingData = YES;
    
    __weak __typeof(self) weakSelf = self;
    [self requestApiDataWithActionType:KSRequestActionTypeLoadMore refreshComplete:^(KSRefreshResult result) {
        __strong __typeof(weakSelf) strongSelf = weakSelf;
        if ([strongSelf.delegate respondsToSelector:@selector(refreshComplete:)]) {
            [strongSelf.delegate refreshComplete:result];
        }
        strongSelf.isRequestingData = NO;
    }];
}

#pragma mark - Request methods

- (void)requestApiDataWithActionType:(KSRequestActionType)actionType refreshComplete:(KSRefreshComplete)refreshComplete {
    self.requestActionType = actionType;
    
    KSRequestApi *api;
    if ([self.delegate respondsToSelector:@selector(requestApiWithCurrentPagingCursor:)]) {
        api = [self.delegate requestApiWithCurrentPagingCursor:self.pagingCursor];
    }
    
    if (api == nil) return;
    
    [api startWithCompletionBlockWithSuccess:^(KSRequestApi *request) {
        if (request.dataJSONError == nil) {
            NSArray *array;
            if ([self.delegate respondsToSelector:@selector(modelsTransformerFromfromJSONDictionary:)]) {
                array = [self.delegate modelsTransformerFromfromJSONDictionary:request.dataJSONObject];
                if (actionType == KSRequestActionTypeRefresh) {
                    [self.dataSource removeAllObjects];
                }
                if (actionType == self.requestActionType) {
                    [self.dataSource addObjectsFromArray:array];
                }
            }
            
            if (array.count > 0) {
                if ([self.delegate respondsToSelector:@selector(updatePagingCursorWithLatestObject:)]) {
                    self.pagingCursor = [self.delegate updatePagingCursorWithLatestObject:array.lastObject];
                }
                refreshComplete(KSRefreshResultSuccess);
            } else {
                refreshComplete(KSRefreshResultNoMoreData);
            }
        } else {
            refreshComplete(KSRefreshResultFailure);
        }
    } failure:^(KSRequestApi *request) {
        refreshComplete(KSRefreshResultFailure);
    }];
}

#pragma mark - Data accessors

- (NSMutableArray *)dataSource {
    if (_dataSource == nil) {
        _dataSource = [NSMutableArray arrayWithCapacity:30];
    }
    return _dataSource;
}

@end
